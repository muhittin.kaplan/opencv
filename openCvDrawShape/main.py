import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2BGRA)
    #drawLine
    img = cv2.line(gray, (0, 0), (128, 128), (0, 0, 0), 5)
    #DrawRectangle
    img = cv2.rectangle(img, (384, 0), (510, 128), (0, 255, 0), 3)
    #drawCşrcle
    img = cv2.circle(img, (447, 63), 63, (0, 0, 255), -1)
    #drawEllipse
    img = cv2.ellipse(img, (256, 256), (100, 50), 0, 0, 180, 255, -1)
    #polygon
    pts = np.array([[10, 5], [20, 30], [70, 20], [50, 10]], np.int32)
    pts = pts.reshape((-1, 1, 2))
    img = cv2.polylines(img, [pts], True, (0, 255, 255))
    #Text
    font = cv2.FONT_HERSHEY_PLAIN
    cv2.putText(img, 'Muhittin KAPLAN', (10, 200), font, 4, (255, 255, 255), 2, cv2.LINE_AA)

    cv2.imshow('frame',img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


cap.release()
cv2.destroyAllWindows()