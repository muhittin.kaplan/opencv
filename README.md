```bash
# openCvLearn
.
├── openCvBasicOperation
│   ├── img.png
│   └── main.py
├── openCvCaptureCam
│   └── main.py
├── openCvDrawShape
│   └── main.py
├── openCvOpenImageMatplotlib
│   ├── img.png
│   └── main.py
├── openCvReadImage
│   ├── img.png
│   └── main.py
├── openCvTrackbar
│   └── main.py
└── README.md
```
