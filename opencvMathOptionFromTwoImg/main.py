import cv2

img1=cv2.imread("img.png")
img2=cv2.imread("img2.png")
dst = cv2.addWeighted(img1,0.9,img2,0.1,0)

cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
