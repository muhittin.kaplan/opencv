'''
Access pixel values and modify them
Access image properties
Setting Region of Image(ROI)
Splitting and Merging images
'''

import cv2
import numpy as np

img=cv2.imread("img.png")
px=img[100,100]
print(px) #Blue, Green, Red

#access only blue pxel
blue = img[100,100,0]
print(blue)

#changePixel
img[100,100] = [255,255,255]
print (img[100,100])

for col in range (200,300):
    for row in range(200,300):
        img[col,row]=[255,0,0]

# accessing RED value
print(img.item(10,10,2))
# modifying RED value
img.itemset((10,10,2),100)
print(img.item(10,10,2))
print (img.shape)#img size

'''
#all white
for i in range (img.shape[0]):
    for j in range(img.shape[1]):
        img[i,j]=[255,255,255]
'''
#kesit almak
area = img[280:340, 330:390]#belirtilen bölgeyi
img[273:333, 100:160] = area#belirtilen noktaya taşıyor

#split
b,g,r = cv2.split(img)
img = cv2.merge((b,g,r))

cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()